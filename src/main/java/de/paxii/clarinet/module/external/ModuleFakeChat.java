package de.paxii.clarinet.module.external;

import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;
import de.paxii.clarinet.util.chat.ChatColor;

/**
 * Created by Lars on 08.05.2016.
 */
public class ModuleFakeChat extends Module {
  public ModuleFakeChat() {
    super("FakeChat", ModuleCategory.OTHER);

    this.setDisplayedInGui(false);
    this.setCommand(true);
    this.setVersion("1.0");
    this.setBuildVersion(15801);
    this.setDescription("Displays a fake Chatmessage");
    this.setSyntax("fakechat <message>");
  }

  @Override
  public void onCommand(String[] args) {
    StringBuilder sb = new StringBuilder();
    for (String part : args)
      sb.append(part).append(" ");

    String chatMessage = ChatColor.translateAlternateColorCodes('&', sb.toString().trim());
    Chat.printChatMessage(chatMessage);
  }
}
